﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordGen {
    public partial class PasswordGen : Form {
        // Class variables
        char[] upper =   {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        char[] lower =   {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        char[] symbols = {'@', '#', '$', '%', '&', '*', '(', ')', '-', '_'};

        Random rand = new Random();

        // Event handlers
        private void generate_btn_Click(object sender, EventArgs e) {
            status_bar.Value = 0;

            string password = "";

            List<char> choices = new List<char>();

            if(lower_box.Checked) {
                choices.AddRange(lower);
            }
            if(upper_box.Checked) {
                choices.AddRange(upper);
            }
            if(num_box.Checked) {
                choices.AddRange(numbers);
            }
            if(sym_box.Checked) {
                choices.AddRange(symbols);
            }
            if(include_text.Text.Length > 0) {
                choices.AddRange(include_text.Text.ToCharArray());
            }

            if(exclude_text.Text.Length > 0) {
                for(int i = 0; i < exclude_text.Text.Length; i++) {
                    choices.RemoveAll(item => item == exclude_text.Text[i]);
                }
            }

            char next;
            for(int i = 0; i < (int)passlength_select.Value; i++) {
                if(choices.Count > 0) {
                    next = choices[rand.Next(choices.Count)];
                } else {
                    next = ' ';
                }

                if(repeat_box.Checked) {
                    choices.RemoveAll(item => item == next);
                }

                password += next;
            }

            status_bar.Value = 100;
            pass_text.Text = password;
        }
        private void copy_btn_Click(object sender, EventArgs e) {
            Clipboard.SetText(pass_text.Text);
        }

        // Constructor
        public PasswordGen() {
            InitializeComponent();
        }
    }
}
