﻿namespace PasswordGen {
    partial class PasswordGen {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswordGen));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.passlength_select = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.exclude_text = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.include_text = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.sym_box = new System.Windows.Forms.CheckBox();
            this.upper_box = new System.Windows.Forms.CheckBox();
            this.lower_box = new System.Windows.Forms.CheckBox();
            this.num_box = new System.Windows.Forms.CheckBox();
            this.pass_text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.generate_btn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.status_bar = new System.Windows.Forms.ToolStripProgressBar();
            this.copy_btn = new System.Windows.Forms.Button();
            this.repeat_box = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passlength_select)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.repeat_box);
            this.groupBox1.Controls.Add(this.passlength_select);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.exclude_text);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.include_text);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.sym_box);
            this.groupBox1.Controls.Add(this.upper_box);
            this.groupBox1.Controls.Add(this.lower_box);
            this.groupBox1.Controls.Add(this.num_box);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 177);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // passlength_select
            // 
            this.passlength_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.passlength_select.Location = new System.Drawing.Point(364, 66);
            this.passlength_select.Name = "passlength_select";
            this.passlength_select.Size = new System.Drawing.Size(59, 20);
            this.passlength_select.TabIndex = 10;
            this.passlength_select.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(146, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password Length:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(166, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(258, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Leave all options checked for most secure password.";
            // 
            // exclude_text
            // 
            this.exclude_text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exclude_text.Location = new System.Drawing.Point(200, 41);
            this.exclude_text.Name = "exclude_text";
            this.exclude_text.Size = new System.Drawing.Size(223, 20);
            this.exclude_text.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(146, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Exclude:";
            // 
            // include_text
            // 
            this.include_text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.include_text.Location = new System.Drawing.Point(200, 18);
            this.include_text.Name = "include_text";
            this.include_text.Size = new System.Drawing.Size(223, 20);
            this.include_text.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Include:";
            // 
            // sym_box
            // 
            this.sym_box.AutoSize = true;
            this.sym_box.Checked = true;
            this.sym_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sym_box.Location = new System.Drawing.Point(7, 90);
            this.sym_box.Name = "sym_box";
            this.sym_box.Size = new System.Drawing.Size(65, 17);
            this.sym_box.TabIndex = 3;
            this.sym_box.Text = "Symbols";
            this.sym_box.UseVisualStyleBackColor = true;
            // 
            // upper_box
            // 
            this.upper_box.AutoSize = true;
            this.upper_box.Checked = true;
            this.upper_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.upper_box.Location = new System.Drawing.Point(7, 44);
            this.upper_box.Name = "upper_box";
            this.upper_box.Size = new System.Drawing.Size(82, 17);
            this.upper_box.TabIndex = 2;
            this.upper_box.Text = "Upper Case";
            this.upper_box.UseVisualStyleBackColor = true;
            // 
            // lower_box
            // 
            this.lower_box.AutoSize = true;
            this.lower_box.Checked = true;
            this.lower_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lower_box.Location = new System.Drawing.Point(7, 20);
            this.lower_box.Name = "lower_box";
            this.lower_box.Size = new System.Drawing.Size(82, 17);
            this.lower_box.TabIndex = 1;
            this.lower_box.Text = "Lower Case";
            this.lower_box.UseVisualStyleBackColor = true;
            // 
            // num_box
            // 
            this.num_box.AutoSize = true;
            this.num_box.Checked = true;
            this.num_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.num_box.Location = new System.Drawing.Point(7, 67);
            this.num_box.Name = "num_box";
            this.num_box.Size = new System.Drawing.Size(68, 17);
            this.num_box.TabIndex = 0;
            this.num_box.Text = "Numbers";
            this.num_box.UseVisualStyleBackColor = true;
            // 
            // pass_text
            // 
            this.pass_text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pass_text.Location = new System.Drawing.Point(121, 198);
            this.pass_text.Name = "pass_text";
            this.pass_text.Size = new System.Drawing.Size(160, 20);
            this.pass_text.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Password:";
            // 
            // generate_btn
            // 
            this.generate_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generate_btn.Location = new System.Drawing.Point(368, 196);
            this.generate_btn.Name = "generate_btn";
            this.generate_btn.Size = new System.Drawing.Size(75, 23);
            this.generate_btn.TabIndex = 3;
            this.generate_btn.Text = "Generate";
            this.generate_btn.UseVisualStyleBackColor = true;
            this.generate_btn.Click += new System.EventHandler(this.generate_btn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.status_bar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 227);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(455, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // status_bar
            // 
            this.status_bar.Name = "status_bar";
            this.status_bar.Size = new System.Drawing.Size(100, 16);
            // 
            // copy_btn
            // 
            this.copy_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copy_btn.Location = new System.Drawing.Point(287, 196);
            this.copy_btn.Name = "copy_btn";
            this.copy_btn.Size = new System.Drawing.Size(75, 23);
            this.copy_btn.TabIndex = 5;
            this.copy_btn.Text = "Copy";
            this.copy_btn.UseVisualStyleBackColor = true;
            this.copy_btn.Click += new System.EventHandler(this.copy_btn_Click);
            // 
            // repeat_box
            // 
            this.repeat_box.AutoSize = true;
            this.repeat_box.Location = new System.Drawing.Point(7, 130);
            this.repeat_box.Name = "repeat_box";
            this.repeat_box.Size = new System.Drawing.Size(83, 17);
            this.repeat_box.TabIndex = 12;
            this.repeat_box.Text = "No Repeats";
            this.repeat_box.UseVisualStyleBackColor = true;
            // 
            // PasswordGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 249);
            this.Controls.Add(this.copy_btn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.generate_btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pass_text);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PasswordGen";
            this.Text = "PasswordGen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passlength_select)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox pass_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button generate_btn;
        private System.Windows.Forms.CheckBox sym_box;
        private System.Windows.Forms.CheckBox upper_box;
        private System.Windows.Forms.CheckBox lower_box;
        private System.Windows.Forms.CheckBox num_box;
        private System.Windows.Forms.TextBox exclude_text;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox include_text;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown passlength_select;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar status_bar;
        private System.Windows.Forms.Button copy_btn;
        private System.Windows.Forms.CheckBox repeat_box;
    }
}